(function(){//Scope de js


//Nombres de contenedores en el DOM
var users_list = 'userslist';
var name_field = 'name_field';
var users_anim = 'usersanim';
var btn_add = 'btn_add';
var btn_go = 'btn_go';

//Nombres de usuarios
var names = [];

window.onload = function() {
	main();
};

function main(){
	//alert("Full load page DOM 2");
	addEmptyTemplate();
	document.getElementById("btn_add").focus();
	document.getElementById( btn_add ).addEventListener('click' , addName );
	document.getElementById( btn_go ).addEventListener('click' , startAnimation );
	document.getElementById( name_field ).addEventListener('keydown', function( e ){
		if( e.keyCode == '13' )
			addName();
	})
}

function getRandom(){
    return Math.floor(Math.random() * names.length );
}

function addEmptyTemplate(){
	document.getElementById('userslist').innerHTML = "<div id='empty-user'><span>Agrega a una victima :)</span></div>"
}

function deleteUser(){
	/*
		Encontrar usuairo
		Eliminar usuario
		Verificar si el listado se encuentra vacio.
	 */
}

/*
	params: null
	return: null
*/
function addName(){

	var name = document.getElementById( name_field ).value;
	if( name.length == 0 || name == "") return;

	//verificamos si se encuentran registrodos usuario
	if(names.length == 0){
		document.getElementById('empty-user').className = "fadeOut";
		var temp = setTimeout(function(){
			document.getElementById('empty-user').className += " hidden";
		}, 1000);
	}

	//Obtenemos la posicion a insertar
	var pos = names.length;

	//Agregamos el nombre en el arreglo
	names.push( name );
	addCard( name , pos );
	document.getElementById( pos + '_s-card').className += " fadeIn";


	document.getElementById( name_field ).value = "";
	return;
}

function addCard( name , index ){
	document.getElementById('userslist').innerHTML += getCard( name , index , true );
}


/*
	params 	: String : name -> nombre de usuario
			: integer : index -> posicion de usuario
			: boolean : type -> typo de tarjeta
	return  : string : template de datos para insertar en DOM
*/
function getCard( name , index , type ){
	var clase = ( type )? "s-card" : "a_card";
	var template = 	"<div id='"  + index + "_" + clase + "' class='" + clase + " row'>" +
					    "<div class='col-xs-3 col-sm-3 col-md-3 col-lg-3'>" +
					        "<div class='circle white'>" +
					            "<span class='number-circle'>" +
					                index +
					            "</span>" +
					        "</div>" +
					    "</div>" +
					    "<div class='col-xs-7 col-sm-7 col-md-7 col-lg-7 name_box'>" +
					        "<span class='text-card c-white'>" +
					            name +
					        "</span>" +
					    "</div>" +
					    "<div class='col-xs-2 col-sm-2 col-md-2 col-lg-2 close-btn'>" +
					        "<span class='c-yellow'>x</span>" +
					    "</div>" +
					"</div>";
	return template;
}

/*
	Funcion de animacion
 */
function startAnimation(){
	if( names.length < 2 ) return;

	var index = getRandom();

	if( document.getElementById('usersanim').className == 'setWinner' ){
		document.getElementById('usersanim').className = "hidden";
		document.getElementById('btn_go').innerHTML = "Go Again!!";
	}else{

		document.getElementById('target_winner').innerHTML = getWinner( names[index] , index );
		document.getElementById('usersanim').className = "setWinner";
		document.getElementById('btn_go').innerHTML = "Reiniciar ? ";
	}
	//document.getElementById('usersanim').className = "hidden";
	return;
}

/*
	Retorna el template de car ganador
	params : String : nombre de usuario ganador
			: integer : indice de posicion en arreglo
	return : String : template ganador
 */
function getWinner( name , index ){
	return 	"<div>" + 
		        "<span class='namewinner'>" + name + "</span>" + 
		    "</div>" + 
		    "<div class='numberwinner'>" +
		        index + 
		    "</div>";
}


})()